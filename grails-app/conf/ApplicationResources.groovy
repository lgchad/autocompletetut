modules = {
    application {
        resource url:'js/application.js'
    }

    util{
        resource url:'js/util.js'
    }
}