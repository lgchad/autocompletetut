import com.icodeya.Country

class BootStrap {

    def init = { servletContext ->
        if (!Country.count()) {
            new Country(name: "China").save(flush: true, failOnError: true)
            new Country(name: "South Korea").save(flush: true, failOnError: true)
            new Country(name: "Japan").save(flush: true, failOnError: true)
            new Country(name: "Thailand").save(flush: true, failOnError: true)
            new Country(name: "Philippines").save(flush: true, failOnError: true)
        }
    }
    def destroy = {
    }
}
