$(document).ready(function() {
    $.ajax({
        type: "GET",
        url: "/autoCompleteTut/country/getAllCountries",
        success : function(response) {

            $("#country_textField").autocomplete({
                source: response
            });
        }
    });

});